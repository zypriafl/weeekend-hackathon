import Vue from 'vue'
import Router from 'vue-router'
import KpiScreen from '@/pages/KpiScreen'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'KpiScreen',
      component: KpiScreen
    }
  ]
})
