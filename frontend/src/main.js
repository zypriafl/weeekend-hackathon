// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/DataStore'
import VueFlex from 'vue-flex'
// Already autoprefixed for vendor prefixes.
// Also namespaced to avoid collisions.
import 'vue-flex/dist/vue-flex.css'

Vue.config.productionTip = false
Vue.use(VueFlex)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
