import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

let MeanRevenueTypes = {
  LASTYEAR: 'Letztes Jahr',
  LASTTHREEYEARS: 'Letzte 3 Jahre'
}

let Markets = {
  KERAMIK: 'Keramik Anlagenbau',
  BRANDSCHUTZ: 'Brandschutz',
  FOOD: 'Food Ingredients',
  BAUCHEMIE: 'Bauchemie',
  MASTERBATCHES: 'Masterbatches',
  LACKE: 'Antimikrobielle Lacke und Farben',
  SCHMUCK: 'Modeechtschmuck'
}

const store = new Vuex.Store({
  state: {
    markets: Markets,
    meanRevenueTypes: MeanRevenueTypes,
    selectedMarket: Markets.MASTERBATCHES,
    selectedMeanRevenueType: MeanRevenueTypes.LASTYEAR,
    AverageEmployeesKpi: 50,
    AverageEmployeesKpiChart: {
      type: 'bar',
      data: {
        datasets: [
          {
            label: 'Ø Mitarbeiter',
            type: 'bar',
            data: [50],
            backgroundColor: 'rgba(0, 33, 95,.5)',
            borderColor: '#00215f',
            borderWidth: 2
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        lineTension: 1,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
    averageRevenueKpi: {
      type: 'bar',
      data: {
        datasets: [
          {
            label: 'Ø Umsatz [Mio €]',
            type: 'bar',
            data: [4.8],
            backgroundColor: 'rgba(0, 33, 95,.5)',
            borderColor: '#00215f',
            borderWidth: 2
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        lineTension: 1,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
    averageGrowthKpi: {
      type: 'bar',
      data: {
        datasets: [
          {
            label: 'Ø Umsatz [Mio €]',
            type: 'bar',
            data: [4.8],
            backgroundColor: 'rgba(0, 33, 95,.5)',
            borderColor: '#00215f',
            borderWidth: 2
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        lineTension: 1,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
    averageMarginKpi: {
      type: 'bar',
      data: {
        datasets: [
          {
            label: 'Ø Umsatz [Mio €]',
            type: 'bar',
            data: [4.8],
            backgroundColor: 'rgba(0, 33, 95,.5)',
            borderColor: '#00215f',
            borderWidth: 2
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        lineTension: 1,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    }
  }
})

export default store
