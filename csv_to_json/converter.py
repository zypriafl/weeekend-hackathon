import csv
import json

result_dict = {}
csvfile = open('input.csv', 'r', encoding="utf-8-sig", )
jsonfile = open('output.json', 'w')

input_dict = csv.DictReader(csvfile, delimiter=';')
# print('Headers in file: {}'.format(input_dict.fieldnames))

for row in input_dict:
    company_name = row['Unternehmen']

    if not company_name:
        continue

    if company_name not in result_dict:
        # print('New Company detected: {}.'.format(company_name))
        result_dict[company_name] = {}

    current_year_str = row['Jahr X']
    try:
        current_year = int(current_year_str)
    except ValueError:
        current_year = None

    result_dict[company_name]['name'] = row['Unternehmen']

    if row['Markt'] and row['Markt'] != '':
        result_dict[company_name]['market'] = row['Markt']

    if row['Produktportfolio'] and row['Produktportfolio'] != '':
            result_dict[company_name]['description'] = row['Produktportfolio']

    if row['# MA'] and row['# MA'] != '':
        result_dict[company_name]['number_employees'] = row['# MA']

    if current_year:
        result_dict[company_name]['revenues'] = {}

        if row['Umsatz [€Mio] x'] and row['Umsatz [€Mio] x'] != '':
            result_dict[company_name]['revenues'][current_year] = row['Umsatz [€Mio] x']
        if row['Umsatz [€Mio] x-1'] and row['Umsatz [€Mio] x-1'] != '':
            result_dict[company_name]['revenues'][current_year - 1] = row['Umsatz [€Mio] x-1']
        if row['Umsatz [€Mio] x-2'] and row['Umsatz [€Mio] x-2'] != '':
            result_dict[company_name]['revenues'][current_year - 2] = row['Umsatz [€Mio] x-2']

        if row['EBIT [€Mio]'] and row['EBIT [€Mio]'] != '':
            result_dict[company_name]['ebit'] = {}
            result_dict[company_name]['ebit'][current_year] = row['EBIT [€Mio]']

result_list = [value for key, value in result_dict.items()]
json.dump(result_list, jsonfile, ensure_ascii=False)
jsonfile.write('\n')
print('{} companies written to json-output.'.format(len(result_dict.keys())))


# Example JSON-Format for Companies:
# {
#     name: '',
#     market: '',
#     description: '',
#     founded_at: 1989,
#     number_employees: 50,
#     revenues: [{
#             year: 2015,
#             value_in_million: 0.25
#         },
#         {
#             year: 2016,
#             value_in_million: 0.5
#         }
#     ],
#     ebit: [{
#             year: 2015,
#             value_in_million: 0.25
#         }
#     ]
# }
