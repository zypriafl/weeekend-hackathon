# -*- coding: utf-8 -*-
"""
Provides Data Access Object for Company.
"""

#  standard imports
from datetime import datetime
# third party imports
from elasticsearch_dsl import DocType, Date, Keyword, Text, Double, Integer, Nested, InnerDoc


# application imports


class YearValue(InnerDoc):
    value = Double()
    year = Integer()


class CompanyDAO(DocType):
    """Data Access Object for Companies"""
    created_at = Date()

    name = Text(analyzer='snowball', fields={'raw': Keyword()})
    market = Text(analyzer='snowball', fields={'raw': Keyword()})
    description = Text(analyzer='snowball', fields={'raw': Keyword()})

    number_employees = Integer()

    growths = Nested(YearValue)
    margins = Nested(YearValue)

    ebits = Nested(YearValue)
    revenues = Nested(YearValue)

    class Meta:
        index = 'company'
