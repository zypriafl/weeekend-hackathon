# -*- coding: utf-8 -*-
"""
Provides Transfer Object for Company.
"""

#  standard imports
# third party imports
from marshmallow import Schema, fields
# application imports

class EbitSchema(Schema):
    ebit = fields.Float()
    year = fields.Int()


class Margin(Schema):
    margin = fields.Float()
    year = fields.Int()

class RevenueSchema(Schema):
    revenue = fields.Float()
    year = fields.Int()


class RevenueGrowthSchema(Schema):
    revenue_growth = fields.Float()
    year = fields.Int()


class CompanySchema(Schema):
    _id = fields.String()
    name = fields.String()
    ebit = fields.Nested(EbitSchema, many=False)
    revenues = fields.Nested(RevenueSchema, many=True)
    revenue_growths = fields.Nested(RevenueGrowthSchema, many=True)
    margin = fields.Nested(Margin, many=False)
    products = fields.Nested(fields.String)






