# -*- coding: utf-8 -*-
"""
Provides mapper functionality for companies.
"""


# standard imports
from enum import Enum
# third party imports
from elasticsearch_dsl import Q
# application imports
from mergerspot.dealsourcing.data_model import CompanyDAO
from mergerspot.dealsourcing.domain_model import CompanySchema


class CompanyMapper(object):

    class CompanyQueryTypes(Enum):
        by_id = 0,
        by_name = 1

    def get_companies_by_market(self, market):
        companies = self._query_by_market(market)
        return companies
        #return CompanySchema().loads(companies, many=True)

    def get_company(self, query_type, data):
        switch = {
            CompanyMapper.CompanyQueryTypes.by_id: self.__query_by_id,
            CompanyMapper.CompanyQueryTypes.by_name: self.__query_by_name,
        }
        company_data = switch[query_type](data)
        if company_data is not None:
            if type(company_data) == CompanyDAO:
                return CompanySchema().dump(company_data)
            else:
                return CompanySchema().dump(company_data, many=True)
        else:
            return None

    def __query_by_id(self, company_id):
        return CompanyDAO.get(id=company_id, ignore=404)

    def __query_by_name(self, search_name):
        search = CompanyDAO.search()
        search.query = Q('match', name=search_name)
        return search.execute().hits

    def _query_by_market(self, search_market):
        search = CompanyDAO.search()
        search.query = Q('match', market=search_market)
        return search.execute().hits
