# -*- coding: utf-8 -*-
"""
Provides Transfer Object for Company.
"""

#  standard imports
# third party imports
from marshmallow import Schema, fields
# application imports


class KpiSchema(Schema):
    name = fields.String()
    year = fields.Integer(required=False)
    value = fields.Float()

    def __str__(self):
        return '{}, {}, {}'.format(self.name, self.year, self.value)

    def __repr__(self):
        return '{}, {}, {}'.format(self.name, self.year, self.value)

class KpiCollection(Schema):
    kpis = fields.Nested(KpiSchema, many=True)








