# -*- coding: utf-8 -*-
"""
Provides Transfer Object for Company.
"""

#  standard imports
from enum import Enum
# third party imports
# application imports
from mergerspot.dealsourcing.domain_model.kpi_schema import KpiSchema, KpiCollection
from mergerspot.dealsourcing.domain_model import CompanyMapper


class KpiParameters(Enum):
    REVENUES = 'revenues'
    REVENUE_GROWTHS = 'revenue_growths'
    EBIT = 'ebit'
    MARGIN = 'margin'


class KpiCalculation:

    def calculate_all_kpis(self, market) -> list:
        market_companies = CompanyMapper().get_companies_by_market(market)
        all_kpis = []
        all_kpis.append(self.generate_average_employee_kpi(market_companies))
        all_kpis += (self.generate_average_kpis_per_year(market_companies, "revenues"))
        all_kpis += (self.generate_average_kpis_per_year(market_companies, "ebits"))
        #all_kpis += (self.generate_average_kpis_per_year(market_companies, "growths"))
        all_kpis += (self.generate_market_growth_per_year(market_companies))
        all_kpis += (self.generate_average_kpis_per_year(market_companies, "margins"))
        return KpiSchema().dumps(all_kpis, many=True)

    def generate_average_employee_kpi(self, market_companies: list) -> KpiSchema:
        sum_of_employees = 0
        number_of_companies = 0
        for company in market_companies:
            if company.number_employees:
                sum_of_employees += company.number_employees
                number_of_companies += 1
        average_employee_number = KpiSchema()
        average_employee_number.name = "average employee number"
        average_employee_number.value = 0
        if number_of_companies is not 0:
            average_employee_number.value = sum_of_employees / float(number_of_companies)
        return average_employee_number

    def generate_market_growth_per_year(self, market_companies:list) -> list:
        years = []
        for company in market_companies:
            years = self.get_list_of_years_for_kpi(years, getattr(company, "revenues"))
        overall_revenue = {}
        for year in years:
            overall_revenue[year] = 0
            for company in market_companies:
                for each_revenue in company.revenues:
                    if each_revenue.year == year:
                        overall_revenue[year] += each_revenue.value
        market_growth = []
        for year, value in overall_revenue.items():
            try:
                previous_year_revenue = overall_revenue[year-1]
            except KeyError:
                continue
            if previous_year_revenue != 0:
                growth_value = (value-previous_year_revenue)/previous_year_revenue
                years_growth= KpiSchema()
                years_growth.name = "revenue_growths"
                years_growth.year = year
                years_growth.value = growth_value
                market_growth.append(years_growth)
        return market_growth


    def generate_average_kpis_per_year(self, market_companies: list, parameters) -> list:
        years = []
        for company in market_companies:
            years = self.get_list_of_years_for_kpi(years, getattr(company, parameters))
        parameter_kpis = []
        for year in years:
            parameter_kpis.append(self.get_kpi_for_year(year, market_companies, parameters))
        return parameter_kpis

    def get_list_of_years_for_kpi(self, years: list, kpi_value: list) -> list:
        for parameter in kpi_value:
            if parameter.year not in years:
                years.append(parameter.year)
        return years

    def get_kpi_for_year(self, year, market_companies: list, parameters):
        number_of_companies = 0
        sum_of_parameter = 0
        for company in market_companies:
            for parameter in getattr(company, parameters):
                if parameter.year == year:
                    sum_of_parameter += parameter.value
                    number_of_companies += 1
        average_parameter = KpiSchema()
        average_parameter.name = "average" + parameters
        average_parameter.year = year
        average_parameter.value = sum_of_parameter / float(number_of_companies)
        return average_parameter
