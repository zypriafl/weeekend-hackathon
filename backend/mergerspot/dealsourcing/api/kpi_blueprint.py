# -*- coding: utf-8 -*-
"""
Defines routes for the kpi calculation class.
"""


#  standard imports
# third party imports
from flask import Blueprint, jsonify
# application imports
from mergerspot.dealsourcing.domain_model import KpiCalculation


KpiBlueprint = Blueprint('kpis', __name__)


@KpiBlueprint.route('/all/<market>', methods=['GET'])
def get_kpis_by_market(market):
    "Return various KPIs for the queried market"
    status_code = 404
    response_data = 'No corresponding KPIs were found for the queried market'
    kpis = KpiCalculation().calculate_all_kpis(market)
    if kpis is not None:
        response_data = kpis
        status_code = 200
    return response_data

