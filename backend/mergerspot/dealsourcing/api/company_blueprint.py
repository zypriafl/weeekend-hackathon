# -*- coding: utf-8 -*-
"""
Defines routes for the company class.
"""

#  standard imports
# third party imports
import json
from datetime import datetime

from flask import Blueprint, jsonify, request, abort
# application imports
from mergerspot.dealsourcing.data_model import CompanyDAO
from mergerspot.dealsourcing.data_model.company_dao import YearValue
from mergerspot.dealsourcing.domain_model import CompanyMapper

CompanyBlueprint = Blueprint('companies', __name__)


@CompanyBlueprint.route('/name/<name>', methods=['GET'])
def get_company_by_name(name):
    """Return detailed data of the company that matches the query name."""
    status_code = 404
    response_data = 'No company matching the name was found.'
    company = CompanyMapper().get_company(CompanyMapper.CompanyQueryTypes.by_name, name)
    if company is not None:
        response_data = company.data
        status_code = 200
    return jsonify(response_data), status_code


@CompanyBlueprint.route('/id/<company_id>', methods=['GET'])
def get_company_by_id(company_id):
    """Return detailed data of the company that matches the query id."""
    status_code = 404
    response_data = 'No company matching the ID was found.'
    company = CompanyMapper().get_company(CompanyMapper.CompanyQueryTypes.by_id, company_id)
    if company is not None:
        response_data = company.data
        status_code = 200
    return jsonify(response_data), status_code


def create_company(company_dict):
    name = company_dict.get('name', None)
    description = company_dict.get('description', None)
    market = company_dict.get('market', None)

    try:
        number_employees = int(company_dict['number_employees'])
    except (ValueError, KeyError):
        number_employees = None

    doc = CompanyDAO(
        name=name,
        market=market,
        description=description,
        number_employees=number_employees,
        created_at=datetime.now()
    )

    # Add Nested Fields
    for (json_field, dao_field) in [('revenues', 'revenues'), ('ebit', 'ebits')]:
        if json_field in company_dict and company_dict[json_field]:
            for key, value in company_dict[json_field].items():
                try:
                    float_value = float(value.replace(',', '.'))
                    int_year = int(key)
                except ValueError:
                    continue

                doc_nested_field = getattr(doc, dao_field)
                doc_nested_field.append(
                    YearValue(value=float_value, year=int_year)
                )

                if json_field == 'revenues':
                    # Write Margin when updating revenues
                    try:
                        ebit_value = float(company_dict['ebit'][key].replace(',', '.'))
                    except (ValueError, KeyError):
                        ebit_value = None

                    if ebit_value:
                        doc.margins.append(
                            YearValue(value=ebit_value / float_value, year=int_year)
                        )

                if json_field == 'revenues':
                    # Write revenue growth
                    # Try to get previous year revenues
                    try:
                        ebit_dict = company_dict.get('revenues', None)
                        print(ebit_dict)
                        if ebit_dict:
                            previous_ebit = ebit_dict.get(str(int_year - 1), None)
                        else:
                            previous_ebit = None
                    except KeyError:
                        previous_ebit = None

                    if previous_ebit:
                        try:
                            previous_ebit = float(previous_ebit.replace(',', '.'))
                        except ValueError:
                            previous_ebit = None

                    if previous_ebit:
                        current_ebit = float_value
                        growth = (current_ebit-previous_ebit)/previous_ebit
                        doc.growths.append(
                            YearValue(value=growth, year=int_year)
                        )

    doc.save()


@CompanyBlueprint.route('/add', methods=['POST'])
def post_companies():
    """Add new Company Data."""

    if not request.json:
        abort(400)

    for company_dict in request.json:
        create_company(company_dict)

    return json.dumps({'status': '{} companies created/updated.'.format(len(request.json))}), 201
