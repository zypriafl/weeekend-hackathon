# -*- coding: utf-8 -*-
"""

Author: Ingo Mayer

Standard functionality for Elasticsearch
"""


#  standard imports
# third party imports
from elasticsearch_dsl import connections
# application imports


class ElasticSearchConnector(object):
    connected = False

    def __init__(self, database_host: str):
        connections.create_connection('default', hosts=[database_host], sniff_on_start=False)
        try:
            connections.get_connection()
            self.connected = True
        except KeyError:
            self.connected = False

