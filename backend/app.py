# -*- coding: utf-8 -*-
"""
Entry point for the Flask Application.
"""


#  standard imports
import os
# third party imports
from flask_cors import CORS
from flask_api import FlaskAPI
# mergerspot imports
from mergerspot.dealsourcing.api import CompanyBlueprint, KpiBlueprint
from mergerspot.elasticsearch_connector import ElasticSearchConnector

App = FlaskAPI(__name__, instance_relative_config=True)
# set config
app_settings = os.getenv('APP_SETTINGS')
App.config.from_object(app_settings)
# set routes
CORS(App)
App.register_blueprint(CompanyBlueprint, url_prefix="/companies")
App.register_blueprint(KpiBlueprint, url_prefix="/kpis")
# setup database connection
elastic_client = ElasticSearchConnector('https://search-dealsourcing-dev-7utl662adlmqqovidoenbx47wm.eu-central-1.es.amazonaws.com')


@App.route('/')
def life_signal():
    return 'Welcome to dealsourcing-service!'


if __name__ == "__main__":
    App.run()
