# MergerSpot - Flask Microservice Template
This repository serves as a functional boilerplate for MergerSpot Flask applications.
Simply clone/download the repository and start coding.

## Setup 
1. Install virtualenv for Python3.6 with 'pip3 install virtualenv'
2. Navigate into root directory and create virtual env with 'virtualenv venv'
3. Setup virtual environment as project Python configuration
4. Install dependencies from requirements.txt

## Development
1. Rename the 'microservice' sub-directory (in mergerspot/) to the service's name


### General Remarks
* Use the \_\_init\_\_.py files for readable imports. See the blueprint module and its import in app.py as example.
* Use standard Python3 (PEP8) naming conventions

### Routes
* Place all routes in the api folder. See the api directory for an example
* Register blueprints with a prefix. See app.py for an example

### Data Modelling
ToDo

## Usage
### Local Development Application
Execute these commands in the terminal:
export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run --host 0.0.0.0

The application can now be reached under localhost:5000

### Docker 
ToDo

## Testing 
All tests are located in the /tests directory.
* The test files need to be named 'test_*'
* The test classes must inherit from TestBase
* the test functions must be named 'test_*' with descriptive names. 
* See test_example.py for an example implementation. Especially the 'actual_response', 'expected response' convention should be followed.

Execute tests with 'python -m unittest discover'.
Single tests can be run with 'python -m unittest test.test_module_name'.