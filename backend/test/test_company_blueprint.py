# -*- coding: utf-8 -*-
"""
Unit Tests for the Cluster Blueprint.
"""


# standard imports
import unittest
import unittest.mock as mock
import json
# third party imports
# application imports
from test.test_base import TestBase
from mergerspot.dealsourcing.data_model import CompanyDAO
from mergerspot.dealsourcing.domain_model import CompanyMapper


test_name = 'Test GmbH'
test_id = 2


def mock_query(self, user_input):
    return [CompanyDAO(meta={'id': test_id}, name=test_name)]


class TestClusterBlueprint(TestBase):
    @mock.patch.object(CompanyMapper, '_CompanyMapper__query_by_name', mock_query)
    def test_company_route_with_name_returns_correct_number_companies(self):
        query_string = '/companies/name/' + test_name
        result = json.loads(self.test_app.get(query_string).data)
        expected_value = 1
        actual_value = len(result)
        self.assertEqual(expected_value, actual_value)

    @mock.patch.object(CompanyMapper, '_CompanyMapper__query_by_id', mock_query)
    def test_company_route_with_id_returns_correct_number_companies(self):
        query_string = '/companies/id/' + str(test_id)
        result = json.loads(self.test_app.get(query_string).data)
        expected_value = 1
        actual_value = len(result)
        self.assertEqual(expected_value, actual_value)


if __name__ == '__main__':
    unittest.main()
