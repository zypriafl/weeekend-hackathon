# -*- coding: utf-8 -*-
"""
Unit Tests Base Class. Extend this in all test classes.
"""


# standard imports
# third party imports
import unittest
# application imports
import app


class TestBase(unittest.TestCase):
    def setUp(self):
        app.App.testing = True
        self.test_app = app.App.test_client()


if __name__ == '__main__':
    unittest.main()
