# -*- coding: utf-8 -*-
"""
Unit Tests for the Cluster Blueprint.
"""


# standard imports
import unittest
# third party imports
# application imports
from test.test_base import TestBase
from app import elastic_client


class TestDatabase(TestBase):
    def test_database_connection_active(self):
        expected_value = True
        actual_value = elastic_client.connected
        self.assertEqual(expected_value, actual_value)


if __name__ == '__main__':
    unittest.main()
