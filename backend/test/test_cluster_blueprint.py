# -*- coding: utf-8 -*-
"""
Unit Tests for the Cluster Blueprint.
"""


# standard imports
import unittest
import unittest.mock as mock
import json
# third party imports
# application imports
from test.test_base import TestBase
from mergerspot.dealsourcing import ClusterSearchEngine
from mergerspot.dealsourcing.data_model import ClusterDAO


tested_keywords = ['eins', 'zwei', 'drei']


def mock_retrieve_matching_clusters(self):
    return [ClusterDAO(name='TestCluster', keywords=tested_keywords)]


class TestClusterBlueprint(TestBase):
    @mock.patch.object(ClusterSearchEngine, 'retrieve_matching_clusters', mock_retrieve_matching_clusters)
    def test_cluster_route_returns_correct_number_of_clusters(self):
        # double json import necessary to convert returned route data to list
        query_string = '/clusters/'
        for keyword in tested_keywords:
            query_string += (keyword + ',')
        result = json.loads(json.loads(self.test_app.get(query_string).data))
        expected_value = 1
        actual_value = len(result)
        self.assertEqual(expected_value, actual_value)


if __name__ == '__main__':
    unittest.main()
